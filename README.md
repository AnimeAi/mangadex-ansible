# mangadex-ansible

Small Ansible role & playbook for setting up the Mangadex @ Home client.

You will need to set the client secret for this playbook to work.

Install Mangadex client on the remote host: `ansible-playbook -i host.example.com, mdath.yml`

TODO:
- Use [variable precedence](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable) with git ignore or another method to persist client key without repo conflicts
- Upgrade tag and tasks
- More variables for settings